﻿namespace finternational_common.Enums
{
    public enum LeagueType
    {
        Auction = 1,
        Draft = 2
    }
}
