﻿namespace finternational_common.Enums
{
    public enum PositionEnum
    {
        Goalkeeper = 1,
        Defender = 2,
        Midfielder = 3,
        Attacker = 4
    }
}
