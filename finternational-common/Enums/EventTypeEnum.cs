﻿namespace finternational_common.Enums
{
    public enum EventTypeEnum
    {
        SixtyMinutes = 1,
        SixtyPlusMinutes = 2,
        GoalkeeperGoal = 3,
        DefenderGoal = 4,
        MidfielderGoal = 5,
        AttackerGoal = 6,
        Assist = 7,
        GoalkeeperCleanSheet = 8,
        DefenderCleanSheet = 9,
        MidfielderCleanSheet = 10,
        Saves = 11,
        PenaltySave = 12,
        PenaltyMiss = 13,
        Conceded = 14,
        YellowCard = 16,
        RedCard = 17,
        OwnGoal = 18,
        PenaltyWon = 19
    }
}