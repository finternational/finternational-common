﻿namespace finternational_common.Enums
{
    public enum MessageTypeEnum
    {
        UpdateEntries = 1,
        InsertPerformances = 2
    }
}
