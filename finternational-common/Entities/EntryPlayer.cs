﻿namespace finternational_common.Entities
{
    public record EntryPlayer(
        bool Captain, 
        bool ViceCaptain, 
        bool Substitute, 
        int PlayerId,
        int SquadPosition,
        Player? Player = null, 
        string? Opponent = null,
        int Points = 0)
    {
    }
}
