﻿namespace finternational_common.Entities
{
    public class GameInfo
    { 
        public int GameId { get; set; }
        public int HomeGoals { get; set; }
        public int AwayGoals { get; set; }
        public List<GamePlayer> OwnedPlayers { get; set; }
        public List<GameEvent> GameEvents { get; set; }

    }
}
