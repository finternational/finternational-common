﻿using Microsoft.Data.SqlClient;

namespace finternational_common.Entities
{
    public record Game(
        int Id,
        int? HomeId,
        string HomeName,
        int HomeGoals,
        int? AwayId,
        string AwayName,
        int AwayGoals,
        DateTime KickOff,
        bool Finished,
        DateTime? NextCheck,
        DateTime? FinishedTime,
        bool? FinishedProvisional,
        bool TransfersReversed)
    {
        public static Game Read(SqlDataReader reader)
        {
            return new Game(
                reader.GetInt32(0),
                reader.IsDBNull(1) ? null : reader.GetInt32(1),
                reader.IsDBNull(2) ? String.Empty : reader.GetString(2),
                reader.GetInt32(3),
                reader.IsDBNull(4) ? null : reader.GetInt32(4),
                reader.IsDBNull(5) ? String.Empty : reader.GetString(5),
                reader.GetInt32(6),
                reader.GetDateTime(7),
                reader.GetBoolean(8),
                reader.IsDBNull(9) ? null : reader.GetDateTime(9),
                reader.IsDBNull(10) ? null : reader.GetDateTime(10),
                reader.IsDBNull(11) ? null : reader.GetBoolean(11),
                reader.GetBoolean(12));
        }
    }
}
