﻿using Microsoft.Data.SqlClient;

namespace finternational_common.Entities
{
    public record GameEvent(int EventType, string PlayerFirstName, string PlayerLastName, int CountryId)
    {
        public static GameEvent Read(SqlDataReader reader)
        {
            return new GameEvent(
                reader.GetInt32(0),
                reader.GetString(1),
                reader.GetString(2),
                reader.GetInt32(3));
        }
    }
}
