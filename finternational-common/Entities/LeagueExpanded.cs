﻿namespace finternational_common.Entities
{
    public class LeagueExpanded
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Standing> Standings { get; set; }
    }
}
