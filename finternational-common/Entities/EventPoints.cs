﻿using Microsoft.Data.SqlClient;

namespace finternational_common.Entities
{
    public record EventPoints(int Id, string Name, int Points, int? Position)
    {
        public static EventPoints Read(SqlDataReader reader)
        {
            return new EventPoints(
                reader.GetInt32(0),
                reader.GetString(1),
                reader.GetInt32(2),
                reader.IsDBNull(3) ? null : reader.GetInt32(3));
        }
    }
}
