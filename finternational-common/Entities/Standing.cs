﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace finternational_common.Entities
{
    public record Standing(Guid TeamId, string TeamName, string FirstName, string LastName, int Points, int GameweekPoints) { }
}
