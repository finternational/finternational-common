﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace finternational_common.Entities
{
    public record Country(int Id, string Name)
    {
    }
}
