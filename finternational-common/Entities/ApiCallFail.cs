﻿namespace finternational_common.Entities
{
    public class ApiCallFail
    {
        public string Url { get; set; }
        public int StatusCode { get; set; }
        public string Information { get; set; }
    }
}
