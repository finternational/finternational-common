﻿using Microsoft.Data.SqlClient;

namespace finternational_common.Entities
{
    public record Event(int Id, int Type, int Player, int Country, int Game)
    {
        public static Event Read(SqlDataReader reader)
        {
            return new Event(reader.GetInt32(0), reader.GetInt32(1), reader.GetInt32(2), reader.GetInt32(3), reader.GetInt32(4));
        }
    }
}
