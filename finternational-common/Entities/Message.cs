﻿using finternational_common.Enums;

namespace finternational_common.Entities
{
    public class Message
    {
        public Message(int Id, string Data, MessageTypeEnum Type, DateTime? Processed, DateTime Created)
        {
            this.Id = Id;
            this.Data = Data;
            this.Type = Type;
            this.Processed = Processed;
            this.Created = Created;
        }

        public Message(string Data, MessageTypeEnum Type) 
        {
            this.Data = Data;
            this.Type = Type;
        }

        public int Id { get; set; }
        public string Data { get; set; }
        public MessageTypeEnum Type { get; set; }
        public DateTime? Processed { get; set; }
        public DateTime Created { get; set; }
    }
}
