﻿namespace finternational_common.Entities
{
    public class Entry
    {
        public int Id { get; set; }
        public Guid TeamId { get; set; }
        public int GameweekId { get; set; }

        public string TeamName { get; set; }
        public string ManagerFirstName { get; set; }
        public string ManagerLastName { get; set; }
        public List<EntryPlayer> Players { get; set; }
    }
}
