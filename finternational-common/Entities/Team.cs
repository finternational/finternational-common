﻿using Microsoft.Data.SqlClient;

namespace finternational_common.Entities
{
    public record Team(Guid Id, string TeamName, string ManagerFirstName, string ManagerLastName, int? League)
    {
        public static Team Read(SqlDataReader reader)
        {
            return new Team(
                reader.GetGuid(0), 
                reader.GetString(1),
                reader.GetString(2), 
                reader.GetString(3),
                reader.IsDBNull(4) ? null : reader.GetInt32(4));
        }
    }
}
