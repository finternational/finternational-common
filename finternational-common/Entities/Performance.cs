﻿using Microsoft.Data.SqlClient;

namespace finternational_common.Entities
{
    public record Performance(
        int Id,
        int Player,
        int Game,
        int Minutes,
        int Conceded,
        int Points,
        int Goals,
        int PenaltyWon,
        int PenaltyMissed,
        int PenaltySaved,
        int Assists,
        int YellowCard,
        int RedCard,
        int OwnGoal,
        int Saves,
        bool CleanSheet,
        int? PlayerExternalId)
    {
        public static Performance Read(SqlDataReader reader)
        {
            return new Performance(
                reader.GetInt32(0),
                reader.GetInt32(1),
                reader.GetInt32(2),
                reader.GetInt32(3),
                reader.GetInt32(4),
                reader.GetInt32(5),
                reader.GetInt32(6),
                reader.GetInt32(7),
                reader.GetInt32(8),
                reader.GetInt32(9),
                reader.GetInt32(10),
                reader.GetInt32(11),
                reader.GetInt32(12),
                reader.GetInt32(13),
                reader.GetInt32(14),
                reader.GetBoolean(15),
                reader.IsDBNull(16) ? null : reader.GetInt32(16));
        }
    }
}
