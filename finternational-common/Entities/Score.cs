﻿using Microsoft.Data.SqlClient;

namespace finternational_common.Entities
{
    public record Score(int HomeGoals, int AwayGoals)
    {
        public static Score Read(SqlDataReader reader)
        {
            return new Score(reader.GetInt32(0), reader.GetInt32(1));
        }
    }
}
