﻿using Microsoft.Data.SqlClient;

namespace finternational_common.Entities
{
    public record GamePlayer(
        int CountryId,
        string PlayerFirstName,
        string PlayerSecondName,
        string ManagerFirstName,
        string ManagerSecondName,
        Guid TeamId,
        bool Substitute,
        bool Captain,
        int Points)
    {
        public static GamePlayer Read(SqlDataReader reader)
        {
            return new GamePlayer(
                reader.GetInt32(0),
                reader.GetString(1),
                reader.GetString(2),
                reader.GetString(3),
                reader.GetString(4),
                reader.GetGuid(5),
                reader.GetBoolean(6),
                reader.GetBoolean(7),
                reader.GetInt32(8));
        }
    }
}
