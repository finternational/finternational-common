﻿using Microsoft.Data.SqlClient;

namespace finternational_common.Entities
{
    public record League(int Id, string Name, bool IsPrivate, int LeagueType, Guid JoinCode, Guid Creator, bool PlayersPicked) 
    {
        public static League Read(SqlDataReader reader)
        {
            return new League(
                reader.GetInt32(0),
                reader.GetString(1),
                reader.GetBoolean(2),
                reader.GetInt32(3),
                reader.GetGuid(4),
                reader.GetGuid(5),
                reader.GetBoolean(6));
        }

        public static League Create(int id, string name, bool isPrivate, int leagueType, Guid creator)
        {
            return new League(id, name, isPrivate, leagueType, Guid.NewGuid(), creator, false);
        }
    }
}
