﻿using Microsoft.Data.SqlClient;

namespace finternational_common.Entities
{
    public record ExtendedPerformance(
        int Gameweek,
        int? PlayerTeamGoals,
        int? OppositionGoals,
        int? Minutes,
        int? Conceded,
        int? Points,
        int? GameId,
        string OppositionName,
        int? Goals,
        int? PenaltyWon,
        int? PenaltyMissed,
        int? PenaltySaved,
        int? Assists,
        int? YellowCard,
        int? RedCard,
        int? OwnGoal,
        int? Saves,
        bool? CleanSheet)
    {
        public static ExtendedPerformance Read(SqlDataReader reader)
        {
            return new ExtendedPerformance(
                reader.GetInt32(0),
                reader.IsDBNull(1) ? null : reader.GetInt32(1),
                reader.IsDBNull(2) ? null : reader.GetInt32(2),
                reader.IsDBNull(3) ? null : reader.GetInt32(3),
                reader.IsDBNull(4) ? null : reader.GetInt32(4),
                reader.IsDBNull(5) ? null : reader.GetInt32(5),
                reader.IsDBNull(6) ? null : reader.GetInt32(6),
                reader.IsDBNull(7) ? String.Empty : reader.GetString(7),
                reader.IsDBNull(8) ? null : reader.GetInt32(8),
                reader.IsDBNull(9) ? null : reader.GetInt32(9),
                reader.IsDBNull(10) ? null : reader.GetInt32(10),
                reader.IsDBNull(11) ? null : reader.GetInt32(11),
                reader.IsDBNull(12) ? null : reader.GetInt32(12),
                reader.IsDBNull(13) ? null : reader.GetInt32(13),
                reader.IsDBNull(14) ? null : reader.GetInt32(14),
                reader.IsDBNull(15) ? null : reader.GetInt32(15),
                reader.IsDBNull(16) ? null : reader.GetInt32(16),
                reader.IsDBNull(17) ? null : reader.GetBoolean(17));
        }
    }
}
