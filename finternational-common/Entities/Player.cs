﻿namespace finternational_common.Entities
{
    public record Player(int Id, string FirstName, string LastName, int Position, Country Country, string Name, int? ExternalId)
    {
    }
}
