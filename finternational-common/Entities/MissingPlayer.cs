﻿namespace finternational_common.Entities
{
    public record MissingPlayer(string Name, int? Team, int PlayerId)
    {
    }
}
