﻿namespace finternational_common.Entities
{
    public record Gameweek(int Id, bool Current, DateTime Deadline, DateTime End, bool Next, bool Autosubs) { }
}
