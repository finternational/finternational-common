﻿namespace finternational_common.Messaging
{
    public class InsertPerformancesMessage
    {
        public int GameId { get; set; }
        public int? HomeTeam { get; set; }
        public int? AwayTeam { get; set; }
    }
}
