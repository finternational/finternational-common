﻿using finternational_common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace finternational_common.Messaging
{
    public record FinternationalMessage(MessageTypeEnum Type, string Data, int OutboxId){ }
}
