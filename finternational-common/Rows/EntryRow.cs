﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace finternational_common.Rows
{
    public record EntryRow(
        int Id, 
        Guid Team, 
        int Gameweek, 
        int? Player, 
        bool? Captain, 
        bool? ViceCaptain, 
        bool? Substitute,
        int? SquadPosition,
        string FirstName, 
        string LastName, 
        int? Position,
        string Opponent,
        string CountryName,
        int Points,
        string Name,
        int? PlayerExternalId,
        string TeamName,
        string ManagerFirstName,
        string ManagerLastName) { }
}
