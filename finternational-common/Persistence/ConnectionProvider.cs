﻿using finternational_common.Persistence.Interfaces;
using Microsoft.Data.SqlClient;

namespace finternational_common.Persistence
{
    public class ConnectionProvider : IConnectionProvider
    {
        public SqlConnection CreateConnection(string connectionString)
        {
            var connection = new SqlConnection(connectionString);
            return connection;
        }
    }
}
