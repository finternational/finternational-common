﻿using finternational_common.Persistence.Interfaces;

namespace finternational_common.Persistence
{
    public class UnitOfWorkProvider : IUnitOfWorkProvider
    {
        private readonly IConnectionProvider connectionProvider;

        public UnitOfWorkProvider(IConnectionProvider connectionProvider)
        {
            this.connectionProvider = connectionProvider;
        }

        public IUnitOfWork CreateWithOpenTransaction(string connectionString)
        {
            return new UnitOfWork(this.connectionProvider.CreateConnection(connectionString), true);   
        }

        public IUnitOfWork CreateWithoutOpenTransaction(string connectionString)
        {
            return new UnitOfWork(this.connectionProvider.CreateConnection(connectionString), false);
        }
    }
}
