﻿using Microsoft.Data.SqlClient;

namespace finternational_common.Persistence
{
    public record DatabaseCommand(string Sql, List<SqlParameter> Parameters)
    {
    }
}
