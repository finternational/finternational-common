﻿namespace finternational_common.Persistence.Interfaces
{
    public interface IUnitOfWorkProvider
    {
        IUnitOfWork CreateWithOpenTransaction(string connectionString);
        IUnitOfWork CreateWithoutOpenTransaction(string connectionString);
    }
}
