﻿using finternational_common.Persistence.Repositories.Interfaces;
using Microsoft.Data.SqlClient;

namespace finternational_common.Persistence.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ITeamRepository TeamRepository { get; }
        IEntryRepository EntryRepository(Guid teamId, int leagueId);
        IGameweekRepository GameweekRepository(int competitionId);
        IPlayerRepository PlayerRepository(int competitionId);
        IMessageRepository MessageRepository { get; }
        ILeagueExpandedRepository LeagueExpandedRepository { get; }
        ILeagueRepository LeagueRepository { get; }
        IGameRepository GameRepository(int competitionId);
        IGameInfoRepository GameInfoRepository(int competitionId);
        IEventRepository EventRepository(int competitionId);
        IPerformanceRepository PerformanceRepository(int competitionId);
        IApiCallFailRepository ApiCallFailRepository { get; }
        IMissingPlayerRepository MissingPlayerRepository { get; }

        SqlConnection Connection { get; }
        SqlTransaction? Transaction { get; }

        
        void AddCommand(DatabaseCommand command);
        Task Commit();
    }
}
