﻿using Microsoft.Data.SqlClient;

namespace finternational_common.Persistence.Interfaces
{
    public interface IConnectionProvider
    {
        SqlConnection CreateConnection(string connectionString);
    }
}
