﻿using Microsoft.Data.SqlClient;
using System.Text;

namespace finternational_common.Persistence.Interfaces
{
    public interface ICommandExecutor
    {
        Task Execute(IUnitOfWork unitOfWork, string commandText, List<SqlParameter> parameters);
    }
}
