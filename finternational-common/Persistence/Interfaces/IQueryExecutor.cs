﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace finternational_common.Persistence.Interfaces
{
    public interface IQueryExecutor
    {
        Task<List<T>> ExecuteQuery<T>(IUnitOfWork unitOfWork, string queryText, List<SqlParameter> parameters, Func<SqlDataReader, T> read);
        Task<T> ExecuteQuerySingle<T>(IUnitOfWork unitOfWork, string queryText, List<SqlParameter> parameters, Func<SqlDataReader, T> read);
    }
}
