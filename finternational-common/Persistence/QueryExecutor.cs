﻿using finternational_common.Exceptions;
using finternational_common.Persistence.Interfaces;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace finternational_common.Persistence
{
    public class QueryExecutor : IQueryExecutor
    {
        public async Task<List<T>> ExecuteQuery<T>(IUnitOfWork unitOfWork, string queryText, List<SqlParameter> parameters, Func<SqlDataReader, T> read)
        {
            var items = new List<T>();

            using (var command = unitOfWork.Connection.CreateCommand())
            {
                command.CommandText = queryText;

                if (unitOfWork.Transaction != null)
                {
                    command.Transaction = unitOfWork.Transaction;
                }

               foreach(var param in parameters)
               {
                    command.Parameters.Add(param);
               }

                using (SqlDataReader reader = await command.ExecuteReaderAsync())
                {
                    while (reader.Read())
                    {
                        items.Add(read(reader));
                    }
                }
            }
            
            return items;
        }

        public async Task<T> ExecuteQuerySingle<T>(IUnitOfWork unitOfWork, string queryText, List<SqlParameter> parameters, Func<SqlDataReader, T> read)
        {
            using (var command = unitOfWork.Connection.CreateCommand())
            {
                command.CommandText = queryText;

                if (unitOfWork.Transaction != null)
                {
                    command.Transaction = unitOfWork.Transaction;
                }

                foreach (var param in parameters)
                {
                    command.Parameters.Add(param);
                }

                using (SqlDataReader reader = await command.ExecuteReaderAsync())
                {
                    while (reader.Read())
                    {
                        return read(reader);
                    }

                    throw new EntityNotFoundException();
                }
            }
        }
    }
}
