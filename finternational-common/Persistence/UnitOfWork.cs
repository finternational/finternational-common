﻿using finternational_common.Exceptions;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories;
using finternational_common.Persistence.Repositories.Interfaces;
using Microsoft.Data.SqlClient;

namespace finternational_common.Persistence
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private List<DatabaseCommand> Commands { get; set; }

        public UnitOfWork(SqlConnection connection, bool withOpenTransaction)
        {
            this.Commands = new List<DatabaseCommand>();
            this.Connection = connection;
            this.Connection.Open();

            if (withOpenTransaction)
            {
                this.Transaction = connection.BeginTransaction();
            }
        }

        public ITeamRepository TeamRepository => new TeamRepository(this);
        public IEntryRepository EntryRepository(Guid teamId, int leagueId) => new EntryRepository(this, teamId, leagueId);
        public IGameweekRepository GameweekRepository(int competitionId) => new GameweekRepository(this, competitionId);
        public IPlayerRepository PlayerRepository(int competitionId) => new PlayerRepository(this, competitionId);
        public IMessageRepository MessageRepository => new MessageRepository(this);
        public ILeagueExpandedRepository LeagueExpandedRepository => new LeagueExpandedRepository(this);
        public ILeagueRepository LeagueRepository => new LeagueRepository(this);
        public IGameRepository GameRepository(int competitionId) => new GameRepository(this, competitionId);
        public IGameInfoRepository GameInfoRepository(int competitionId) => new GameInfoRepository(this, competitionId);
        public IEventRepository EventRepository(int competitionId) => new EventRepository(this, competitionId);
        public IPerformanceRepository PerformanceRepository(int competitionId) => new PerformanceRepository(this, competitionId);
        public IApiCallFailRepository ApiCallFailRepository => new ApiCallFailRepository(this);
        public IMissingPlayerRepository MissingPlayerRepository => new MissingPlayerRepository(this);

        public SqlConnection Connection { get; }
        public SqlTransaction? Transaction { get; internal set; }

        public void AddCommand(DatabaseCommand command)
        {
            if (command != null)
            {
                Commands.Add(command);
            }
        }

        public async Task Commit()
        {
            if (this.Transaction == null)
            {
                this.Transaction = Connection.BeginTransaction();
            }

            var commandExecutor = new CommandExecutor();

            foreach (var command in Commands)
            {
                try
                {
                    await commandExecutor.Execute(this, command.Sql, command.Parameters);
                }
                catch (SqlException ex)
                {
                    if (ex.Number == 2627)
                    {
                        throw new EntityAlreadyExistsException(); //probably go in the command executor itself
                    }

                    throw ex;
                }
            }

            await this.Transaction.CommitAsync();
        }

        public void Dispose()
        {
            if (Transaction != null)
            {
                Transaction.Dispose();
            }

            this.Connection.Dispose();
        }
    }
}
