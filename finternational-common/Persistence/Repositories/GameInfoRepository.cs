﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Microsoft.Data.SqlClient;

namespace finternational_common.Persistence.Repositories
{
    public class GameInfoRepository : IGameInfoRepository
    {
        private readonly IQueryExecutor queryExecutor;
        private readonly IUnitOfWork unitOfWork;
        private readonly int competitionId;

        public GameInfoRepository(IUnitOfWork unitOfWork, int competitionId)
        {
            this.queryExecutor = new QueryExecutor();
            this.unitOfWork = unitOfWork;
            this.competitionId = competitionId;
        }
    
        public async Task<GameInfo> GetByGameId(int id, int leagueId)
        {
            var gamePlayersTask = GetGamePlayers(id, leagueId);
            var gameEventsTask = GetGameEvents(id);
            var scoreTask = GetScore(id);

            var gamePlayers = await gamePlayersTask;
            var gameEvents = await gameEventsTask;
            var score = await scoreTask;

            return new GameInfo()
            {  
                GameId = id, 
                GameEvents = gameEvents, 
                OwnedPlayers = gamePlayers, 
                HomeGoals = score.HomeGoals, 
                AwayGoals = score.AwayGoals  
            };
        }

        private async Task<List<GamePlayer>> GetGamePlayers(int id, int leagueId)
        {
            string sql = $"SELECT P.Country, P.FirstName, p.LastName, T.ManagerFirstName, T.ManagerLastName, T.Id, Ep.Substitute, Ep.Captain, Ep.Points FROM COMPETITION_{competitionId}.Game AS G " +
                               $"INNER JOIN League_{leagueId}.Entry as E on G.Gameweek = E.Gameweek " +
                               $"INNER JOIN League_{leagueId}.EntryPlayer as EP on E.Id = Ep.Entry " +
                               $"INNER JOIN Competition_{competitionId}.Player as P on P.Id = Ep.Player and(P.Country = G.Home OR P.Country = G.Away) " +
                               $"INNER JOIN Fantasy.Team as T on T.Id = E.Team " +
                               "WHERE G.Id = @gameId";

            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@gameId", id)
            };

            return await this.queryExecutor.ExecuteQuery(this.unitOfWork, sql, parameters, GamePlayer.Read);
        }

        private async Task<List<GameEvent>> GetGameEvents(int id)
        {
            string sql = $"SELECT ET.Id, P.FirstName, P.LastName, E.Country from COMPETITION_{competitionId}.Game as G " +
                               $"INNER JOIN COMPETITION_{competitionId}.Event AS E on G.Id = E.Game " +
                               $"INNER JOIN COMPETITION_{competitionId}.Player as P on P.Id = E.Player " +
                               $"INNER JOIN Fantasy.EventType AS ET on Et.Id = e.[Type] " +
                               $"WHERE G.Id = @gameId";

            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@gameId", id)
            };

            return await this.queryExecutor.ExecuteQuery(this.unitOfWork, sql, parameters, GameEvent.Read);
        }

        private async Task<Score> GetScore(int id)
        {
            string sql = $"SELECT HomeGoals, AwayGoals from COMPETITION_{competitionId}.Game as G " +
                               "WHERE G.Id = @gameId";

            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@gameId", id)
            };

            return await this.queryExecutor.ExecuteQuerySingle(this.unitOfWork, sql, parameters, Score.Read);
        }
    }
}
