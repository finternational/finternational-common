﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Microsoft.Data.SqlClient;

namespace finternational_common.Persistence.Repositories
{
    public class LeagueExpandedRepository : ILeagueExpandedRepository
    {
        private readonly IQueryExecutor queryExecutor;
        private readonly IUnitOfWork unitOfWork;

        public LeagueExpandedRepository(IUnitOfWork unitOfWork)
        {
            this.queryExecutor = new QueryExecutor();
            this.unitOfWork = unitOfWork;
        }

        public static Standing ReadStanding(SqlDataReader reader)
        {
            return new Standing(
                    reader.GetGuid(0),
                    reader.GetString(1),
                    reader.GetString(2),
                    reader.GetString(3),
                    reader.GetInt32(4),
                    reader.GetInt32(5));
        } //move these into the entity?

        public static string ReadLeagueName(SqlDataReader reader)
        {
            return reader.GetString(0);
        }

        public async Task<LeagueExpanded> GetById(int id, int gameweek)
        {
            var nameTask = this.GetLeagueName(id);
            var standingsTask = this.GetStandings(id, gameweek);

            var name = await nameTask;
            var standings = await standingsTask;

            return new LeagueExpanded { Id = id, Name = name, Standings = standings };
        }

        private async Task<string> GetLeagueName(int id)
        {
            const string sql = "SELECT [NAME] from Fantasy.League where Id = @leagueId";

            var parameters = new List<SqlParameter> {
                new SqlParameter("@leagueId", id)
            };

            return await this.queryExecutor.ExecuteQuerySingle(this.unitOfWork, sql, parameters, ReadLeagueName);
        }

        private async Task<List<Standing>> GetStandings(int id, int gameweek)
        {
            string sql = "SELECT Team, TeamName, ManagerFirstName, ManagerLastName, (SELECT SUM(Points) FROM League_{id}.Entry WHERE TEAM = E.team and Gameweek <= @gameweek) as Points, " +
                               $"(select Points FROM League_{id}.Entry WHERE TEAM = E.team and Gameweek = @gameweek) " +
                               "as GameweekPoints " +
                               $"FROM(SELECT Team, row_number() over(partition by TEAM order by team) as RowNum from League_{id}.Entry ) as E " +
                               "INNER JOIN Fantasy.Team AS T ON T.Id = E.Team " +
                               "WHERE RowNum = 1 and league = @leagueId " +
                               "ORDER BY POINTS DESC ";

            var parameters = new List<SqlParameter> {
                new SqlParameter("@leagueId", id),
                new SqlParameter("@gameweek", gameweek)
            };

            return await this.queryExecutor.ExecuteQuery(this.unitOfWork, sql, parameters, ReadStanding);
        }
    }
}
