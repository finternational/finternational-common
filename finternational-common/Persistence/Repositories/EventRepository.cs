﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Microsoft.Data.SqlClient;
using System.Text;

namespace finternational_common.Persistence.Repositories
{
    public class EventRepository : IEventRepository
    {
        private readonly IQueryExecutor queryExecutor;
        private readonly IUnitOfWork unitOfWork;
        private readonly int competitionId;

        public EventRepository(IUnitOfWork unitOfWork, int competitionId)
        {
            this.queryExecutor = new QueryExecutor();
            this.unitOfWork = unitOfWork;
            this.competitionId = competitionId;
        }

        public async Task<List<Event>> GetByPlayerId(int playerId)
        {
            string sql = $"SELECT [ID], [TYPE], [PLAYER], [COUNTRY], [GAME] FROM COMPETITION_{competitionId}.EVENT " +
                               "WHERE Player = @playerId";

            var parameters = new List<SqlParameter> {
                new SqlParameter("@playerId", playerId)
            };

            return await this.queryExecutor.ExecuteQuery(unitOfWork, sql, parameters, Event.Read);
        }

        public async Task<List<EventPoints>> GetEventPoints()
        {
            const string sql = $"SELECT [ID], [Name], [Points], [Position] FROM Fantasy.EventType";

            var parameters = new List<SqlParameter>();

            return await this.queryExecutor.ExecuteQuery(unitOfWork, sql, parameters, EventPoints.Read);
        }

        public async Task DeleteByGameId(int gameId)
        {
            const string sql = "DELETE FROM COMPETITION.EVENT " +
                               "WHERE Game = @gameId";

            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@gameId", gameId)
            };

            unitOfWork.AddCommand(new DatabaseCommand(sql, parameters));
        }

        public async Task Insert(List<Event> events)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append($"INSERT INTO COMPETITION_{competitionId}.[EVENT] ([Type], [Player], [Country], [Game])");
            stringBuilder.Append("VALUES ");

            var parameters = new List<SqlParameter>();

            for(int i = 0; i < events.Count(); i++)
            {
                stringBuilder.Append($"(@type{i}, @player{i}, @country{i}, @game{i}){(i == events.Count - 1 ? "" : ",")}");
                parameters.Add(new SqlParameter($"@type{i}", events[i].Type));
                parameters.Add(new SqlParameter($"@player{i}", events[i].Player));
                parameters.Add(new SqlParameter($"@country{i}", events[i].Country));
                parameters.Add(new SqlParameter($"@game{i}", events[i].Game));
            }

            unitOfWork.AddCommand(new DatabaseCommand(stringBuilder.ToString(), parameters));
        }
    }
}
