﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Microsoft.Data.SqlClient;
using System.Data.SqlTypes;

namespace finternational_common.Persistence.Repositories
{
    public class GameRepository : IGameRepository
    {
        private readonly IQueryExecutor queryExecutor;
        private readonly UnitOfWork unitOfWork;
        private readonly int competitionId;

        public GameRepository(UnitOfWork unitOfWork, int competitionId)
        {
            this.queryExecutor = new QueryExecutor();
            this.unitOfWork = unitOfWork;
            this.competitionId = competitionId;
        }

        public async Task<List<Game>> GetByGameweek(int gameweekId)
        {
            string sql = "SELECT G.Id, g.Home, HC.Name, G.HomeGoals, g.Away, AC.Name, g.AwayGoals, g.KickOff, G.Finished, G.NextCheck, G.FinishedTime, G.FinishedProvisional, G.TransfersReversed " +
                                $"FROM COMPETITION_{competitionId}.Game as G " +
                                $"INNER JOIN COMPETITION_{competitionId}.Country as HC on HC.Id = G.Home " +
                                $"INNER JOIN COMPETITION_{competitionId}.Country as AC on AC.Id = G.Away " +
                                "WHERE G.Gameweek = @gameweekId " +
                                "order by G.KickOff";

            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@gameweekId", gameweekId),
            };

            return await this.queryExecutor.ExecuteQuery(this.unitOfWork, sql, parameters, Game.Read);
        }

        public async Task<List<Game>> GetTodaysGames()
        {

            //refactor this into view, repo deals with view. 
            string sql = "SELECT G.Id, g.Home, HC.Name, G.HomeGoals, g.Away, AC.Name, g.AwayGoals, g.KickOff, G.Finished, G.NextCheck, G.FinishedTime, G.FinishedProvisional, G.TransfersReversed " +
                                $"FROM COMPETITION_{competitionId}.Game as G " +
                                $"INNER JOIN COMPETITION_{competitionId}.Country as HC on HC.Id = G.Home " +
                                $"INNER JOIN COMPETITION_{competitionId}.Country as AC on AC.Id = G.Away " +
                                "WHERE CAST(G.KickOff as DATE) = CAST(GETDATE() as DATE) " +
                                "ORDER BY g.KickOff";

            var parameters = new List<SqlParameter>();

            return await this.queryExecutor.ExecuteQuery(this.unitOfWork, sql, parameters, Game.Read);
        }

        public async Task<List<Game>> GetStartedGames()
        {
            string sql = "SELECT G.Id, g.Home, HC.Name, G.HomeGoals, g.Away, AC.Name, g.AwayGoals, g.KickOff, G.Finished, G.NextCheck, G.FinishedTime, G.FinishedProvisional, G.TransfersReversed " +
                                $"FROM COMPETITION_{competitionId}.Game as G " +
                                $"INNER JOIN COMPETITION_{competitionId}.Country as HC on HC.Id = G.Home " +
                                $"INNER JOIN COMPETITION_{competitionId}.Country as AC on AC.Id = G.Away " +
                                "WHERE GETDATE() > g.KickOff";
            //joins are not needed here, this can be refactored into table

            var parameters = new List<SqlParameter>();

            return await this.queryExecutor.ExecuteQuery(this.unitOfWork, sql, parameters, Game.Read);
        }

        public async Task<List<Game>> Get()
        {
            string sql = "SELECT G.Id, g.Home, HC.Name, G.HomeGoals, g.Away, AC.Name, g.AwayGoals, g.KickOff, G.Finished, G.NextCheck, G.FinishedTime, G.FinishedProvisional, G.TransfersReversed " +
                                $"FROM COMPETITION_{competitionId}.Game as G " +
                                $"INNER JOIN COMPETITION_{competitionId}.Country as HC on HC.Id = G.Home " +
                                $"INNER JOIN COMPETITION_{competitionId}.Country as AC on AC.Id = G.Away";
            //joins are not needed here, this can be refactored into table

            var parameters = new List<SqlParameter>();

            return await this.queryExecutor.ExecuteQuery(this.unitOfWork, sql, parameters, Game.Read);
        }

        public async Task Update(Game game)
        {
            string sql = $"UPDATE COMPETITION_{competitionId}.Game " +
                               "SET Home = @home, Away = @away, KickOff = @kickOff, " +
                               "HomeGoals = @homeGoals, AwayGoals = @awayGoals, Finished = @finished, " +
                               "NextCheck = @nextCheck, FinishedTime = @finishedTime, FinishedProvisional = @finishedProvisional, TransfersReversed = @transfersReversed " +
                               "WHERE Id = @id";

            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@home", game.HomeId),
                new SqlParameter("@away", game.AwayId),
                new SqlParameter("@kickOff", game.KickOff),
                new SqlParameter("@homeGoals", game.HomeGoals),
                new SqlParameter("@awayGoals", game.AwayGoals),
                new SqlParameter("@finished", game.Finished),
                new SqlParameter("@nextCheck", game.NextCheck ?? SqlDateTime.Null),
                new SqlParameter("@finishedTime", game.FinishedTime ?? SqlDateTime.Null),
                new SqlParameter("@finishedProvisional", game.FinishedProvisional ?? SqlBoolean.Null),
                new SqlParameter("@transfersReversed", game.TransfersReversed),
                new SqlParameter("@id", game.Id)
            };

            unitOfWork.AddCommand(new DatabaseCommand(sql, parameters));
        }
    }
}
