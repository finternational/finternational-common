﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Microsoft.Data.SqlClient;
using System.Text;

namespace finternational_common.Persistence.Repositories
{
    public class MissingPlayerRepository : IMissingPlayerRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public MissingPlayerRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork; 
        }

        public async Task Insert(MissingPlayer missingPlayer)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("INSERT INTO [Logging].[MissingPlayer] ([PlayerName], [PlayerTeam], [PlayerApiId]) ");
            stringBuilder.Append("VALUES (@name, @team, @apiId)");

            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@name", missingPlayer.Name),
                new SqlParameter("@team", missingPlayer.Team),
                new SqlParameter("@apiId", missingPlayer.PlayerId)
            };

            unitOfWork.AddCommand(new DatabaseCommand(stringBuilder.ToString(), parameters));
        }
    }
}
