﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Microsoft.Data.SqlClient;

namespace finternational_common.Persistence.Repositories
{
    public class ApiCallFailRepository : IApiCallFailRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public ApiCallFailRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task Insert(ApiCallFail apiCallFail)
        {
            const string sql = "INSERT INTO[Logging].[ApiCallFail]([Url], [Time], [StatusCode], [Information]) " +
                                "VALUES(@url, GETDATE(), @statusCode, @information)";

            var parameters = new List<SqlParameter> {
                new SqlParameter("@url", apiCallFail.Url),
                new SqlParameter("@statusCode", apiCallFail.StatusCode),
                new SqlParameter("@information", apiCallFail.Information)
            };

            unitOfWork.AddCommand(new DatabaseCommand(sql, parameters));
        }
    }
}
