﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Microsoft.Data.SqlClient;

namespace finternational_common.Persistence.Repositories
{
    public class PlayerRepository : IPlayerRepository
    {
        private readonly IQueryExecutor queryExecutor;
        private readonly UnitOfWork unitOfWork;
        private readonly int competitionId;

        public PlayerRepository(UnitOfWork unitOfWork, int competitionId)
        {
            this.competitionId = competitionId;
            this.queryExecutor = new QueryExecutor();
            this.unitOfWork = unitOfWork;
        }
        private static Player Read(SqlDataReader reader)
        {
            return new Player(
                reader.GetInt32(0), 
                reader.GetString(1), 
                reader.GetString(2), 
                reader.GetInt32(3), 
                new Country(reader.GetInt32(4), string.Empty),
                reader.GetString(5),
                reader.IsDBNull(6) ? null : reader.GetInt32(6));
        }

        private static Player ReadWithCountryName(SqlDataReader reader)
        {
            return new Player(
                reader.GetInt32(0),
                reader.GetString(1),
                reader.GetString(2),
                reader.GetInt32(3),
                new Country(reader.GetInt32(4), reader.GetString(5)),
                reader.GetString(6), 
                reader.IsDBNull(7) ? null : reader.GetInt32(7));
        }

        public async Task<List<Player>> FindByIds(List<int> ids)
        {
            //refactor to make this generic at some point
            string sql = "SELECT [Id], [FirstName], [LastName], [Position], [Country], [Name], [ExternalId] " +
                               $"FROM  COMPETITION_{competitionId}.[Player] " +
                               "WHERE Id in (@id0, @id1, @id2, @id3, @id4, @id5, @id6, @id7, @id8, @id9, @id10, @id11, @id12, @id13, @id14)";

            var parameters = new List<SqlParameter>();

            for(int i = 0; i < ids.Count(); i++)
            {
                parameters.Add(new SqlParameter($"@id{i}", ids[i]));
            }

            return await queryExecutor.ExecuteQuery(unitOfWork, sql, parameters, Read);
        }

        public async Task<Player> GetById(int id)
        {
            string sql = "SELECT [Id], [FirstName], [LastName], [Position], [Country], [Name], [ExternalId] " +
                               $"FROM  COMPETITION_{competitionId}.[Player] " +
                               "WHERE Id = @id";

            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@id", id)
            };

            return await queryExecutor.ExecuteQuerySingle(unitOfWork, sql, parameters, Read);
        }

        public async Task<Player> GetByExternalId(int externalId)
        {
            string sql = "SELECT [Id], [FirstName], [LastName], [Position], [Country], [Name], [ExternalId] " +
                               $"FROM  COMPETITION_{competitionId}.[Player] " +
                               "WHERE ExternalId = @id";

            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@id", externalId)
            };

            return await queryExecutor.ExecuteQuerySingle(unitOfWork, sql, parameters, Read);
        }

        public async Task<List<Player>> Get(int pageSize, int pageNumber, int? country, int? position)
        {
            string sql = "SELECT p.[Id], p.[FirstName], p.[LastName], p.[Position], p.[Country], c.[Name], p.[Name], p.[ExternalId] " +
                               $"FROM  COMPETITION_{competitionId}.[Player] as p " +
                               $"INNER JOIN  COMPETITION_{competitionId}.[Country] as c on c.id = p.country " +
                                WhereBuilder(country, position) +
                               "ORDER BY c.[Name], p.Position, p.LastName OFFSET(@pageNumber -1) * @pageSize ROWS FETCH NEXT @pageSize ROWS ONLY";

            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@pageNumber", pageNumber),
                new SqlParameter("@pageSize", pageSize)
            };
   
            if(country != null)
            {
                parameters.Add(new SqlParameter("@country", country));
            }

            if (position != null)
            {
                parameters.Add(new SqlParameter("@position", position));
            }

            return await queryExecutor.ExecuteQuery(unitOfWork, sql, parameters, ReadWithCountryName);
        }

        private string WhereBuilder(int? country, int? position)
        {
            if(country != null && position != null)
            {
                return "WHERE c.Id = @country AND p.Position = @position ";
            }

            if(country != null)
            {
                return "WHERE c.Id = @country ";
            }

            if (position != null)
            {
                return "WHERE p.Position = @position ";
            }

            return "";
        }
    }
}