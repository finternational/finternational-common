﻿using finternational_common.Entities;
using finternational_common.Enums;
using finternational_common.Exceptions;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using finternational_common.Rows;
using Microsoft.Data.SqlClient;

namespace finternational_common.Persistence.Repositories
{

    //this could do with some refactoring, move Reads into classes? EntryPlayer into different repo?
    public class EntryRepository : IEntryRepository
    {
        private readonly IQueryExecutor queryExecutor;
        private readonly IUnitOfWork unitOfWork;
        private readonly Guid teamId;
        private readonly int leagueId;

        public EntryRepository(IUnitOfWork unitOfWork, Guid teamId, int leagueId)
        {
            this.queryExecutor = new QueryExecutor();
            this.unitOfWork = unitOfWork;
            this.teamId = teamId;
            this.leagueId = leagueId;
        }

        public static EntryRow ReadRow(SqlDataReader reader)
        {
            //move into extension attribute for nullcheck
            return new EntryRow(
                reader.GetInt32(0),
                reader.GetGuid(1),
                reader.GetInt32(2),
                reader.IsDBNull(3) ? null : reader.GetInt32(3),
                reader.IsDBNull(4) ? null : reader.GetBoolean(4),
                reader.IsDBNull(5) ? null : reader.GetBoolean(5),
                reader.IsDBNull(6) ? null : reader.GetBoolean(6),
                reader.IsDBNull(7) ? null : reader.GetInt32(7),
                reader.IsDBNull(8) ? String.Empty : reader.GetString(8),
                reader.IsDBNull(9) ? String.Empty : reader.GetString(9),
                reader.IsDBNull(10) ? null : reader.GetInt32(10),
                reader.IsDBNull(11) ? String.Empty : reader.GetString(11),
                reader.IsDBNull(12) ? String.Empty : reader.GetString(12),
                reader.IsDBNull(13) ? 0 : reader.GetInt32(13),
                reader.IsDBNull(14) ? String.Empty : reader.GetString(14),
                reader.IsDBNull(15) ? null : reader.GetInt32(15),
                reader.GetString(16),
                reader.GetString(17),
                reader.GetString(18));
        }

        public static Entry Read(SqlDataReader reader)
        {
            return new Entry()
            { 
                Id = reader.GetInt32(0),
                TeamId = reader.GetGuid(1),
                GameweekId = reader.GetInt32(2)
            };
        }

        public static int ReadIds(SqlDataReader reader)
        {
            return reader.GetInt32(0);
        }

        public async Task CreateEntries()
        {
            string sql = $"INSERT INTO League_{leagueId}.Entry(Team, Gameweek) " +
                               "SELECT @teamId, Id from [Fantasy].[Gameweek] " +
                               "WHERE Deadline > GETDATE()";

            var parameters = new List<SqlParameter> {
                new SqlParameter("@teamId", this.teamId)
            };

            unitOfWork.AddCommand(new DatabaseCommand(sql, parameters));
        }

        public async Task<List<Entry>> GetAllByGameweek(int gameweekId) //this is so bad lol, repo shouldn't take id, also need an 'expanded' entity
        {
            string sql = "SELECT Id, [Team], [Gameweek] " +
                         $"FROM League_{leagueId}.[Entry] " +
                         "WHERE Gameweek = @gameweekId";

            var parameters = new List<SqlParameter> {
                new SqlParameter("@gameweekId", gameweekId),
            };

            return await queryExecutor.ExecuteQuery(unitOfWork, sql, parameters, Read);
        }

        public async Task<Entry> GetByGameweekId(int gameweekId, int competitionId)
        {
            string sql = "SELECT e.Id, e.[Team], e.[Gameweek], ep.[Player], [Captain], [ViceCaptain], [Substitute], [SquadPosition], [FirstName], [LastName], Position, o.Opponent, c.Name, ep.Points, p.Name, p.ExternalId, T.TeamName, T.ManagerFirstName, T.ManagerLastName " +
                               $"FROM League_{leagueId}.Entry as e " +
                               "LEFT JOIN [Fantasy].[Team] AS T on T.Id = @teamId " +
                               $"LEFT JOIN League_{leagueId}.[EntryPlayer] as ep on e.Id = ep.Entry " +
                               $"LEFT JOIN COMPETITION_{competitionId}.[Player] as p on ep.Player = p.Id " +
                               $"LEFT JOIN Competition_{competitionId}.[Country] as c on p.Country = c.Id " +
                               $"LEFT JOIN Competition_{competitionId}.[Game] as G on (c.Id = g.Home or c.Id = g.Away) and g.Gameweek = @gameweekId " +
                               $"LEFT JOIN Competition_{competitionId}.[Opponent] as o on o.Team = p.Country and o.Gameweek = @gameweekId " +
                               "WHERE e.Team = @teamId and e.Gameweek = @gameweekId ";

            var parameters = new List<SqlParameter> {
                new SqlParameter("@teamId", this.teamId),
                new SqlParameter("@gameweekId", gameweekId)
            };

            var entryRows = await queryExecutor.ExecuteQuery(unitOfWork, sql, parameters, ReadRow);

            if(entryRows.Count == 0)
            {
                //how do we handle if the entry should be there, i.e. signup service has failed.
                throw new EntityNotFoundException();
            }

            var entry = new Entry() 
            { 
                Id = entryRows[0].Id, 
                TeamId = entryRows[0].Team, 
                GameweekId = entryRows[0].Gameweek, 
                TeamName = entryRows[0].TeamName,
                ManagerFirstName = entryRows[0].ManagerFirstName,
                ManagerLastName = entryRows[0].ManagerLastName,
                Players = new List<EntryPlayer>() 
            };

            //count should either be 15 entries or 1 by this point
            for(int i = 0; i < entryRows.Count && entryRows.Count > 1; i++)
            {
                try
                {
                    entry.Players.Add(new EntryPlayer
                        (
                            (bool)entryRows[i].Captain,
                            (bool)entryRows[i].ViceCaptain,
                            (bool)entryRows[i].Substitute,
                            (int)entryRows[i].Player,
                            (int)entryRows[i].SquadPosition,
                            new Player(
                                (int)entryRows[i].Player,
                                entryRows[i].FirstName,
                                entryRows[i].LastName,
                                (int)entryRows[i].Position,
                                new Country(1, entryRows[i].CountryName),
                                entryRows[i].Name,
                                entryRows[i].PlayerExternalId),
                            entryRows[i].Opponent,
                            entryRows[i].Points));
                }
                catch(InvalidOperationException)
                {
                    throw new InvalidDataException();
                }
            }

            return entry;
        }

        public async Task<Entry> GetById(int id)
        {
            string sql = "SELECT Id, [Team], [Gameweek] " +
                         $"FROM League_{leagueId}.[Entry] " +
                         "WHERE Id = @entryId";

            var parameters = new List<SqlParameter> {
                new SqlParameter("@entryId", id),
            };

            return await queryExecutor.ExecuteQuerySingle(unitOfWork, sql, parameters, Read);
        }

        public async Task Update(Entry entry)
        {
            foreach(var player in entry.Players)
            {
                string sql = $"if exists (select 1 FROM League_{leagueId}.EntryPlayer with (updlock,serializable) where Player = @playerId and Entry = @entryId) " +
                                    "begin " +
                                    $"   update League_{leagueId}.EntryPlayer " +
                                    "   set Captain = @captain, ViceCaptain = @viceCaptain, Substitute = @substitute, SquadPosition = @squadPosition " +
                                    "   where Player = @playerId and Entry = @entryId " +
                                    "end " +
                                    "else " +
                                    "begin " +
                                    $"	INSERT into League_{leagueId} (Player, [Entry], Captain, ViceCaptain, Substitute, SquadPosition) " +
                                    "	values(@playerId, @entryId, @captain, @viceCaptain, @substitute, @squadPosition) " +
                                    "end";

                var parameters = new List<SqlParameter> {
                    new SqlParameter("@entryId", entry.Id),
                    new SqlParameter("@playerId", player.PlayerId),
                    new SqlParameter("@captain", player.Captain),
                    new SqlParameter("@viceCaptain", player.ViceCaptain),
                    new SqlParameter("@substitute", player.Substitute),
                    new SqlParameter("@squadPosition", player.SquadPosition)
                };

                unitOfWork.AddCommand(new DatabaseCommand(sql, parameters));
            }
        }

        public async Task<List<int>> GetUpcomingEntryIds(int competitionId)
        {
            string sql = $"SELECT e.id from League_{leagueId}.Entry as e " +
                               $"INNER JOIN Competition_{competitionId}.Gameweek as gw on gw.Id = e.Gameweek " +
                               "WHERE team = @teamId and GW.Deadline > GETDATE() AND gw.Next = 0";

            var parameters = new List<SqlParameter> {
                new SqlParameter("@teamId", this.teamId),
            };

            return await queryExecutor.ExecuteQuery(unitOfWork, sql, parameters, ReadIds);
        }
    }
}
