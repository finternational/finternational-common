﻿using finternational_common.Entities;

namespace finternational_common.Persistence.Repositories.Interfaces
{
    public interface ILeagueRepository
    {
        //make all repositories implement a generic interface at some point?
        Task<League> GetById(int id);
        Task<int> GetNextId();
        Task<League> GetByJoinCode(Guid joinCode);
        Task Create(League league);
        Task Update(League league);
    }
}
