﻿using finternational_common.Entities;

namespace finternational_common.Persistence.Repositories.Interfaces
{
    public interface ILeagueExpandedRepository
    {
        Task<LeagueExpanded> GetById(int id, int gameweek);
    }
}
