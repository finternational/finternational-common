﻿using finternational_common.Entities;

namespace finternational_common.Persistence.Repositories.Interfaces
{
    public interface ITeamRepository
    {
        //make all repositories implement a generic interface at some point?
        Task<Team> GetById(Guid id);
        Task Create(Guid id, string teamName, string givenName, string surname);
        Task Update(Guid id, string teamName, int? League);
    }
}
