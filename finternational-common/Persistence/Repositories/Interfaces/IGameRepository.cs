﻿using finternational_common.Entities;

namespace finternational_common.Persistence.Repositories.Interfaces
{
    public interface IGameRepository
    {
        Task<List<Game>> Get();
        Task<List<Game>> GetByGameweek(int gameweekId);
        Task<List<Game>> GetTodaysGames();
        Task<List<Game>> GetStartedGames();
        Task Update(Game game);
    }
}
