﻿using finternational_common.Entities;

namespace finternational_common.Persistence.Repositories.Interfaces
{
    public interface IPerformanceRepository
    {
        Task<List<ExtendedPerformance>> GetByPlayerId(int playerId);
        Task<List<Performance>> GetByGameId(int gameId);
        Task InsertPerformancesForGame(int gameId, int? homeTeam, int? awayTeam);
        Task<List<Performance>> GetByGameweekId(int gameweekId);
        Task Update(Performance performance);

    }
}
