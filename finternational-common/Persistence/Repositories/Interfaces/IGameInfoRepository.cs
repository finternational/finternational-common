﻿using finternational_common.Entities;

namespace finternational_common.Persistence.Repositories.Interfaces
{
    public interface IGameInfoRepository
    {
        Task<GameInfo> GetByGameId(int id, int leagueId);
    }
}
