﻿using finternational_common.Entities;

namespace finternational_common.Persistence.Repositories.Interfaces
{
    public interface IEventRepository
    {
        Task<List<Event>> GetByPlayerId(int playerId);
        Task<List<EventPoints>> GetEventPoints();
        Task DeleteByGameId(int gameId);
        Task Insert(List<Event> events);
    }
}
