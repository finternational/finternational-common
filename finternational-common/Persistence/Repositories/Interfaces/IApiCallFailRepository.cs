﻿using finternational_common.Entities;

namespace finternational_common.Persistence.Repositories.Interfaces
{
    public interface IApiCallFailRepository
    {
        Task Insert(ApiCallFail apiCallFail);
    }
}
