﻿using finternational_common.Entities;

namespace finternational_common.Persistence.Repositories.Interfaces
{
    public interface IGameweekRepository
    {
        Task<List<Gameweek>> Get(int pageSize, int pageNumber);
        Task<Gameweek> GetById(int id);
        Task<Gameweek> GetByEntryId(int entryId, int leagueId);
        Task<Gameweek> GetCurrentGameweek();
        void Update(Gameweek gameweek);
    }
}
