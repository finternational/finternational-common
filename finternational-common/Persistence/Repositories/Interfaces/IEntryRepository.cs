﻿using finternational_common.Entities;

namespace finternational_common.Persistence.Repositories.Interfaces
{
    public interface IEntryRepository
    {
        Task<Entry> GetById(int id);
        Task CreateEntries();
        Task<Entry> GetByGameweekId(int gameweekId, int competitionId);
        Task Update(Entry entry);
        Task<List<int>> GetUpcomingEntryIds(int competitionId);
        Task<List<Entry>> GetAllByGameweek(int gameweekId);
    }
}
