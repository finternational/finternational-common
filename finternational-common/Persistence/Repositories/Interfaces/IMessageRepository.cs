﻿using finternational_common.Entities;

namespace finternational_common.Persistence.Repositories.Interfaces
{
    public interface IMessageRepository
    {
        Task Insert(Message message);
        Task SetMessageBatchAsProcessed(List<int> ids);
        Task<List<Message>> GetUnprocessedMessages();
    }
}
