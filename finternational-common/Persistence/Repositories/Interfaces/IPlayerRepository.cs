﻿using finternational_common.Entities;

namespace finternational_common.Persistence.Repositories.Interfaces
{
    public interface IPlayerRepository
    {
        Task<List<Player>> FindByIds(List<int> ids);
        Task<List<Player>> Get(int pageSize, int pageNumber, int? Country, int? Position);
        Task<Player> GetById(int id);
        Task<Player> GetByExternalId(int externalId);
    }
}
