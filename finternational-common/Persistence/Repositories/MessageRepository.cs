﻿using finternational_common.Entities;
using finternational_common.Enums;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Microsoft.Data.SqlClient;

namespace finternational_common.Persistence.Repositories
{
    public class MessageRepository : IMessageRepository
    { 
        private readonly IQueryExecutor queryExecutor;
        private readonly IUnitOfWork unitOfWork;

        public MessageRepository(IUnitOfWork unitOfWork)
        {
            this.queryExecutor = new QueryExecutor();
            this.unitOfWork = unitOfWork;
        }

        public static Message Read(SqlDataReader reader)
        {
            //move into extension attribute for nullcheck
            return new Message(
                reader.GetInt32(0),
                reader.GetString(1),
                (MessageTypeEnum)reader.GetInt32(2),
                reader.IsDBNull(3) ? null : reader.GetDateTime(3),
                reader.GetDateTime(4));
        }


        public async Task<List<Message>> GetUnprocessedMessages()
        {
            const string sql = "SELECT [Id], [DATA], [TYPE], [PROCESSED], [CREATED] " +
                               "FROM [outbox].[Message] " +
                               "WHERE [Processed] is null";

            return await this.queryExecutor.ExecuteQuery(this.unitOfWork, sql, new List<SqlParameter>(), Read);
        }

        public async Task SetMessageBatchAsProcessed(List<int> ids)
        {
            string idText = "";
            var parameters = new List<SqlParameter>();
            for (int i = 0; i < ids.Count(); i++)  //move this into helper method same code is used several times
            {
                string idParameterText = $"@id{i}";

                parameters.Add(new SqlParameter(idParameterText, ids[i]));

                if (i != ids.Count() - 1)
                {
                    idParameterText += ", ";
                }

                idText += idParameterText;
            }

            string sql = "UPDATE [outbox].[Message] " +
                         "SET [PROCESSED] = GETDATE() " +
                        $"WHERE [Id] in ({idText}) ";

            unitOfWork.AddCommand(new DatabaseCommand(sql, parameters));
        }

        public async Task Insert(Message message)
        {
            string sql = "INSERT INTO [Outbox].[Message](Data, Type, Created) " +
                         "VALUES(@data, @type, GETDATE())";

            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@data", message.Data),
                new SqlParameter("@type", (int)message.Type)
            };

            unitOfWork.AddCommand(new DatabaseCommand(sql, parameters));
        }
    }
}
