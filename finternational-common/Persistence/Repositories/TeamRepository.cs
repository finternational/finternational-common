﻿using finternational_common.Entities;
using finternational_common.Exceptions;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Microsoft.Data.SqlClient;
using System.Text;

namespace finternational_common.Persistence.Repositories
{
    public class TeamRepository : ITeamRepository
    {
        private readonly IQueryExecutor queryExecutor;
        private readonly UnitOfWork unitOfWork;

        public TeamRepository(UnitOfWork unitOfWork)
        {
            this.queryExecutor = new QueryExecutor();
            this.unitOfWork = unitOfWork;
        }

        public async Task<Team> GetById(Guid id)
        {
            const string sql = "SELECT Id, TeamName, ManagerFirstName, ManagerLastName, League from Fantasy.Team where Id = @id";
            var parameters = new List<SqlParameter> { new SqlParameter("@id", id) };

            return await this.queryExecutor.ExecuteQuerySingle(this.unitOfWork, sql, parameters, Team.Read);
        }

        public async Task Create(Guid id, string teamName, string givenName, string surname)
        {
            const string sql = "Insert into Fantasy.Team([Id], TeamName, ManagerFirstName, ManagerLastName) " +
                                 "values (@id, @teamName, @managerFirstName, @managerLastName)";

            var parameters = new List<SqlParameter> {
                new SqlParameter("@id", id),
                new SqlParameter("@teamName", teamName),
                new SqlParameter("@managerFirstName", givenName),
                new SqlParameter("@managerLastName", surname)
            };

            unitOfWork.AddCommand(new DatabaseCommand(sql.ToString(), parameters));
        }

        public async Task Update(Guid id, string teamName, int? league)
        {
            var sql = new StringBuilder();

            sql.AppendLine("Update Fantasy.Team");
            sql.AppendLine("Set TeamName = @teamName, League = @league");
            sql.AppendLine("Where Id = @id");

            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@id", id),
                new SqlParameter("@teamName", teamName),
                new SqlParameter("@league", league)
            };

            unitOfWork.AddCommand(new DatabaseCommand(sql.ToString(), parameters));
        }
    }
}
