﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Microsoft.Data.SqlClient;
using System.Text;

namespace finternational_common.Persistence.Repositories
{
    public class LeagueRepository : ILeagueRepository
    { 
        private readonly IQueryExecutor queryExecutor;
        private readonly IUnitOfWork unitOfWork;

        public LeagueRepository(IUnitOfWork unitOfWork)
        {
            this.queryExecutor = new QueryExecutor();
            this.unitOfWork = unitOfWork;
        }

        public async Task Create(League league)
        {
            var sql = new StringBuilder();

            sql.AppendLine("INSERT INTO Fantasy.League(Id, Name, Private, LeagueType, JoinCode, Creator, PlayersPicked)");
            sql.AppendLine("VALUES(@id, @name, @private, @leagueType, @joinCode, @creator, @playersPicked)");

            var parameters = new List<SqlParameter> {
                new SqlParameter("@id", league.Id),
                new SqlParameter("@name", league.Name),
                new SqlParameter("@private", league.IsPrivate),
                new SqlParameter("@leagueType", league.LeagueType),
                new SqlParameter("@joinCode", league.JoinCode),
                new SqlParameter("@creator", league.Creator),
                new SqlParameter("@playersPicked", league.PlayersPicked)
            };

            unitOfWork.AddCommand(new DatabaseCommand(sql.ToString(), parameters));
        }

        public async Task<League> GetById(int id)
        {
            var sql = new StringBuilder();

            sql.AppendLine("SELECT [Id], [Name], [Private], [LeagueType], [JoinCode], [Creator], [PlayersPicked]");
            sql.AppendLine("FROM [Fantasy].[League]");
            sql.AppendLine("WHERE Id = @id");

            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@id", id)
            };

            return await queryExecutor.ExecuteQuerySingle(unitOfWork, sql.ToString(), parameters, League.Read);
        }

        public async Task<League> GetByJoinCode(Guid joinCode)
        {
            var sql = new StringBuilder();

            sql.AppendLine("SELECT [Id], [Name], [Private], [LeagueType], [JoinCode], [Creator], [PlayersPicked]");
            sql.AppendLine("FROM [Fantasy].[League]");
            sql.AppendLine("WHERE JoinCode = @joinCode");

            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@joinCode", joinCode)
            };

            return await queryExecutor.ExecuteQuerySingle(unitOfWork, sql.ToString(), parameters, League.Read);
        }

        public async Task<int> GetNextId()
        {
            var sql = new StringBuilder();

            sql.AppendLine("SELECT TOP 1([Id] + 1)");
            sql.AppendLine("FROM [Fantasy].[League]");
            sql.AppendLine("ORDER BY [ID] Desc");

            return await queryExecutor.ExecuteQuerySingle(unitOfWork, sql.ToString(), new List<SqlParameter>(), x => x.GetInt32(0));
        }

        public async Task Update(League league)
        {
            var sql = new StringBuilder();

            sql.AppendLine("UPDATE FANTASY.LEAGUE");
            sql.AppendLine("set [Name] = @name, [Private] = @private, LeagueType = @leagueType, Creator = @creator, PlayersPicked = @playersPicked");
            sql.AppendLine("Where Id = @id");

            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@id", league.Id),
                new SqlParameter("@name", league.Name),
                new SqlParameter("@private", league.IsPrivate),
                new SqlParameter("@leagueType", league.LeagueType),
                new SqlParameter("@creator", league.Creator),
                new SqlParameter("@playersPicked", league.PlayersPicked),
            };

            unitOfWork.AddCommand(new DatabaseCommand(sql.ToString(), parameters));
        }
    }
}
