﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Microsoft.Data.SqlClient;
using System.Text;

namespace finternational_common.Persistence.Repositories
{
    public class GameweekRepository : IGameweekRepository
    {
        private readonly IQueryExecutor queryExecutor;
        private readonly UnitOfWork unitOfWork;
        private readonly int competitionId;

        public GameweekRepository(UnitOfWork unitOfWork, int competitionId)
        {
            this.competitionId = competitionId;
            this.queryExecutor = new QueryExecutor();
            this.unitOfWork = unitOfWork;
        }

        private static Gameweek Read(SqlDataReader reader)
        {
            return new Gameweek(
                reader.GetInt32(0), 
                reader.GetBoolean(1), 
                reader.GetDateTime(2), 
                reader.GetDateTime(3),
                reader.GetBoolean(4),
                reader.GetBoolean(5));
        }

        public async Task<List<Gameweek>> Get(int pageSize, int pageNumber)
        {
            string sql = $"SELECT Id, [Current], Deadline, [End], [Next], [AutoSubs] from COMPETITION_{competitionId}.Gameweek " +
                               "ORDER BY Id OFFSET(@pageNumber - 1) * @pageSize ROWS FETCH NEXT @pageSize ROWS ONLY";
            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@pageNumber", pageNumber),
                new SqlParameter("@pageSize", pageSize)
            };

            return await this.queryExecutor.ExecuteQuery(this.unitOfWork, sql, parameters, Read);
        }

        public async Task<Gameweek> GetByEntryId(int entryId, int leagueId)
        {
            string sql = "SELECT gw.[Id], gw.[Current], gw.[Deadline], gw.[End], gw.[Next], gw.[AutoSubs] " +
                               $"FROM COMPETITION_{competitionId} as gw " +
                               $"INNER JOIN League_{leagueId}.Entry as e on e.Gameweek = gw.id " +
                               "WHERE e.Id = @entryId";

            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@entryId", entryId),
            };

            return await this.queryExecutor.ExecuteQuerySingle(this.unitOfWork, sql, parameters, Read);
        }

        public async Task<Gameweek> GetById(int id)
        {
            string sql = $"SELECT Id, [Current], Deadline, [End], [Next], [AutoSubs] from  COMPETITION_{competitionId}.Gameweek " +
                               "WHERE Id = @id";
            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@id", id),
            };

            return await this.queryExecutor.ExecuteQuerySingle(this.unitOfWork, sql, parameters, Read);
        }

        public async Task<Gameweek> GetCurrentGameweek()
        {
            string sql = $"SELECT Id, [Current], Deadline, [End], [Next], [AutoSubs] from COMPETITION_{competitionId}.Gameweek " +
                               "WHERE GETDATE() > Deadline AND [End] > GETDATE()";

            var parameters = new List<SqlParameter>();

            return await this.queryExecutor.ExecuteQuerySingle(this.unitOfWork, sql, parameters, Read);
        }

        public void Update(Gameweek gameweek)
        {
            var sql = new StringBuilder();

            sql.AppendLine($"Update COMPETITION_{competitionId}.Gameweek");
            sql.AppendLine("Set [Current] = @current, Deadline = @deadline, [End] = @end, [Next] = @next, [AutoSubs] = @autoSubs");
            sql.AppendLine("Where Id = @id");

            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@id", gameweek.Id),
                new SqlParameter("@deadline", gameweek.Deadline),
                new SqlParameter("@end", gameweek.End),
                new SqlParameter("@next", gameweek.Next),
                new SqlParameter("@autoSubs", gameweek.Autosubs)
            };

            unitOfWork.AddCommand(new DatabaseCommand(sql.ToString(), parameters));
        }
    }
}
