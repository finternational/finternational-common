﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Microsoft.Data.SqlClient;
using System.Text;

namespace finternational_common.Persistence.Repositories
{
    public class PerformanceRepository : IPerformanceRepository
    {
        private readonly IQueryExecutor queryExecutor;
        private readonly IUnitOfWork unitOfWork;
        private readonly int competitionId;

        public PerformanceRepository(IUnitOfWork unitOfWork, int competitionId)
        {
            this.queryExecutor = new QueryExecutor();
            this.unitOfWork = unitOfWork;
            this.competitionId = competitionId;
        }

        public async Task<List<Performance>> GetByGameId(int gameId)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("SELECT p.[Id], p.Player, p.Game, p.[Minutes], p.Conceded, p.Points, p.Goals, p.PenaltyWon, p.PenaltyMissed, p.PenaltySaved, p.Assists, p.YellowCard, p.RedCard, p.OwnGoal,");
            stringBuilder.AppendLine("Saves, CleanSheet, pl.ExternalId");
            stringBuilder.AppendLine($"FROM COMPETITION_{competitionId}.[Performance] as p");
            stringBuilder.AppendLine($"INNER JOIN COMPETITION_{competitionId}.Player as pl on pl.Id = p.Player");
            stringBuilder.AppendLine("WHERE Game = @gameId");

            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@gameId", gameId)
            };

            return await this.queryExecutor.ExecuteQuery(this.unitOfWork, stringBuilder.ToString(), parameters, Performance.Read);
        }

        public async Task<List<Performance>> GetByGameweekId(int gameweekId)
        {
            StringBuilder stringBuilder = new StringBuilder(); 
            stringBuilder.AppendLine("SELECT p.[Id], p.Player, p.Game, p.[Minutes], p.Conceded, p.Points, p.Goals, p.PenaltyWon, p.PenaltyMissed, p.PenaltySaved, p.Assists, p.YellowCard, p.RedCard, p.OwnGoal,");
            stringBuilder.AppendLine("Saves, CleanSheet, Pl.ExternalId");
            stringBuilder.AppendLine($"FROM Competition_{competitionId}.Gameweek as GW");
            stringBuilder.AppendLine($"INNER JOIN COMPETITION_{competitionId}.Game as G on G.Gameweek = GW.Id AND GW.Id = @gameweekId");
            stringBuilder.AppendLine($"INNER JOIN COMPETITION_{competitionId} as p ON P.Game = G.Id");
            stringBuilder.AppendLine($"INNER JOIN COMPETITION_{competitionId}.Player as pl on pl.Id = p.Player");

            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@gameweekId", gameweekId)
            };

            return await this.queryExecutor.ExecuteQuery(this.unitOfWork, stringBuilder.ToString(), parameters, Performance.Read);
        }

        public async Task<List<ExtendedPerformance>> GetByPlayerId(int playerId)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine($"DECLARE @countryId int = (select Country from  COMPETITION_{competitionId}.Player where[Id] = @playerId)");
            stringBuilder.AppendLine("SELECT Gw.Id, CASE WHEN G.Home = @countryId THEN g.HomeGoals ELSE G.AwayGoals END as PlayerTeamGoals,");
            stringBuilder.AppendLine("CASE WHEN G.Home = @countryId THEN g.AwayGoals ELSE G.HomeGoals END as OppositionGoals,");
            stringBuilder.AppendLine("P.[Minutes], P.Conceded, p.Points, G.Id, Opposition.Name, P.Goals, P.PenaltyWon, P.PenaltyMissed,");
            stringBuilder.AppendLine($"P.PenaltySaved, P.Assists, P.YellowCard, P.RedCard, P.OwnGoal, P.Saves, P.CleanSheet from Competition_{competitionId}.Gameweek as GW");
            stringBuilder.AppendLine($"LEFT JOIN COMPETITION_{competitionId}.Game as G on GW.Id = G.Gameweek and(g.Home = @countryId or g.Away = @countryId)");
            stringBuilder.AppendLine($"LEFT JOIN COMPETITION_{competitionId}.Performance as P on P.Game = g.Id and P.Player = @playerId");
            stringBuilder.AppendLine($"LEFT JOIN COMPETITION_{competitionId}.Country as Opposition on(Opposition.Id = G.Home or Opposition.Id = G.Away) and(Opposition.Id != @countryId)"); 

            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@playerId", playerId)
            };

            return await this.queryExecutor.ExecuteQuery(this.unitOfWork, stringBuilder.ToString(), parameters, ExtendedPerformance.Read);
        }

        public async Task InsertPerformancesForGame(int gameId, int? homeTeam, int? awayTeam)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine($"INSERT INTO COMPETITION_{competitionId}.Performance(Player, Game)");
            stringBuilder.AppendLine($"SELECT Id, @GameId FROM COMPETITION_{competitionId}.Player");
            stringBuilder.AppendLine($"WHERE(Country = @Home OR Country = @Away) AND Id NOT IN(SELECT Player from COMPETITION_{competitionId}.Performance WHERE Game = @GameId)");

            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@GameId", gameId),
                new SqlParameter("@Home", homeTeam ?? 0),
                new SqlParameter("@Away", awayTeam ?? 0)
            };

            unitOfWork.AddCommand(new DatabaseCommand(stringBuilder.ToString(), parameters));
        }

        public async Task Update(Performance performance)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("SET NOCOUNT ON");
            stringBuilder.AppendLine($"UPDATE COMPETITION_{competitionId}.Performance");
            stringBuilder.AppendLine("SET Player = @player, Game = @game, [Minutes] = @minutes, Conceded = @conceded, Points = @points,");
            stringBuilder.AppendLine("Goals = @goals, PenaltyWon = @penaltyWon, PenaltyMissed = @penaltyMissed, PenaltySaved = @penaltySaved,");
            stringBuilder.AppendLine("Assists = @assists, YellowCard = @yellowCard, RedCard = @redCard, OwnGoal = @ownGoal, Saves = @saves, CleanSheet = @cleanSheet");
            stringBuilder.AppendLine("WHERE[Id] = @id");

            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@player", performance.Player),
                new SqlParameter("@game", performance.Game),
                new SqlParameter("@minutes", performance.Minutes),
                new SqlParameter("@conceded", performance.Conceded),
                new SqlParameter("@points", performance.Points),
                new SqlParameter("@goals", performance.Goals),
                new SqlParameter("@penaltyWon", performance.PenaltyWon),
                new SqlParameter("@penaltyMissed", performance.PenaltyMissed),
                new SqlParameter("@penaltySaved", performance.PenaltySaved),
                new SqlParameter("@assists", performance.Assists),
                new SqlParameter("@yellowCard", performance.YellowCard),
                new SqlParameter("@redCard", performance.RedCard),
                new SqlParameter("@ownGoal", performance.OwnGoal),
                new SqlParameter("@saves", performance.Saves),
                new SqlParameter("@id", performance.Id),
                new SqlParameter("@cleanSheet", performance.CleanSheet)
            };

            unitOfWork.AddCommand(new DatabaseCommand(stringBuilder.ToString(), parameters));
        }
    }
}
