﻿using finternational_common.Exceptions;
using finternational_common.Persistence.Interfaces;
using Microsoft.Data.SqlClient;
using System.Text;

namespace finternational_common.Persistence
{
    public class CommandExecutor : ICommandExecutor
    {
        public async Task Execute(IUnitOfWork unitOfWork, string commandText, List<SqlParameter> parameters)
        {
            using (var command = unitOfWork.Connection.CreateCommand())
            {
                command.CommandText = commandText;
                command.Transaction = unitOfWork.Transaction;

                foreach (var param in parameters)
                {
                    command.Parameters.Add(param);
                }

                await command.ExecuteNonQueryAsync();
            }
            //should this be in try catch?
        }


        public async Task<int> Insert(IUnitOfWork unitOfWork, StringBuilder sql, List<SqlParameter> parameters)
        {
            using (var command = unitOfWork.Connection.CreateCommand())
            {
                sql.AppendLine("SELECT CAST(SCOPE_IDENTITY() AS INT)");
                command.CommandText = sql.ToString();
                command.Transaction = unitOfWork.Transaction;

                foreach (var param in parameters)
                {
                    command.Parameters.Add(param);
                }

                try
                {
                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        while (reader.Read())
                        {
                            return reader.GetInt32(0);
                        }

                        throw new InsertFailedException();
                    }
                } 
                catch (Exception ex)
                {
                    throw new InsertFailedException();
                }
            }
        }
    }
}