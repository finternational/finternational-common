﻿using finternational_common.Persistence;
using Microsoft.Data.SqlClient;
using Moq.AutoMock;
using Xunit;

namespace finternational_common_tests.Persistence
{
    public class ConnectionProviderTests
    {
        private AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public void CreateReturnsCorrectResults()
        {
            var sut = this.autoMocker.CreateInstance<ConnectionProvider>();

            var result = sut.CreateConnection("");

            Assert.IsType<SqlConnection>(result);
        }
    }
}
